import * as React from 'react';
import { Question } from './components/Question';
import { questionKey as importedQuestionKey } from './questionKey';
const uuid = require('uuid/v4');

interface QuestionKey {
	question: string;
	answers: [{
		answer: string;
		correct: boolean;
		explanation: string;
	}];
}

const questionKey = importedQuestionKey;

class App extends React.Component {
	render() {
		const questions = questionKey.map((question: QuestionKey) => {
			return (
				<Question
					key={uuid()}
					question={question.question}
					answers={question.answers}
				/>
			);
		});

		return (
			<div className="App">
				<div className="jumbotron">
					<img src="https://www.crosskeys.bank/assets/img/cross-keys-bank.svg" alt="" height="100" />
					<br />
					<br />
					<h3>Complete the following quiz to see just how security savvy you are.</h3>
				</div>
				<div className="container questions">
					{questions}
					<br />
					<br />
				</div>
				<div className="footer">Created by Anthony Autrey, Noah Guillory, Andrew Moreau, and Peter Pham for CINS 3044.</div>
			</div>
		);
	}
}

export default App;
