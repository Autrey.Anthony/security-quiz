import * as React from 'react';
const uuid = require('uuid/v4');

interface QuestionProps {
	question: string;
	answers: [{
		answer: string;
		correct: boolean;
		explanation: string;
	}];
}

interface QuestionState {
	selectedAnswer: number;
	submittedAnswers: Set<number>;
	answerExplanation: string | null;
	answerExplanationClass: string;
}

export class Question extends React.Component<QuestionProps, QuestionState> {
	constructor() {
		super();

		this.state = {
			selectedAnswer: 0,
			submittedAnswers: new Set<number>(),
			answerExplanation: null,
			answerExplanationClass: ''
		};
	}

	handleSelectAnswer(e: any) {
		e.preventDefault();
		this.setState({ selectedAnswer: parseInt(e.target.name, 10) });
	}

	handleSubmitAnswer(e: any) {
		e.preventDefault();
		let color: string = 'incorrect';
		if (this.props.answers[this.state.selectedAnswer].correct)
			color = 'correct';

		let newSubmittedAnswers: Set<number> = this.state.submittedAnswers;
		newSubmittedAnswers.add(this.state.selectedAnswer);
		this.setState({
			submittedAnswers: newSubmittedAnswers,
			answerExplanation: this.props.answers[this.state.selectedAnswer].explanation,
			answerExplanationClass: color
		});
	}

	render() {
		const correctIcon: JSX.Element = <span className="icon correct">&#10004;</span>;
		const incorrectIcon: JSX.Element = <span className="icon incorrect">&#10008;</span>;

		const answers = this.props.answers.map((answer) => {
			let index: number = this.props.answers.indexOf(answer);
			let symbol = answer.correct ? correctIcon : incorrectIcon;

			return (
				<div key={uuid()} >
					{this.state.submittedAnswers.has(index) ?
						symbol :
						<input
							onChange={(e) => this.handleSelectAnswer(e)}
							key={uuid()}
							type="radio"
							name={this.props.answers.indexOf(answer).toString()}
							value={answer.correct.toString()}
							checked={index === this.state.selectedAnswer}
						/>}
					<span className="lMar4">{answer.answer}</span>
				</div>
			);
		});

		let answerExplanation = null;
		if (this.state.answerExplanation !== null)
			answerExplanation = <div className={this.state.answerExplanationClass + ' explanation'}>{this.state.answerExplanation}</div>;

		return (
			<div className="row">
				<div className="question">{this.props.question}</div>
				<form onSubmit={(e) => this.handleSubmitAnswer(e)}>
					{answers}
					{answerExplanation}
					<br />
					<button className="btn btn-success" type="submit" value="Submit Answer">Submit Answer</button>
				</form>
				<hr className="col-sm-8 col-sm-offset-2" />
			</div>
		);
	}
}

export default Question;