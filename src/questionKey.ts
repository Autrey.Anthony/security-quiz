export const questionKey = [{
	question:
	'What is the answer?',
	answers: [
		{
			answer: 'answer 1',
			correct: false,
			explanation: 'answer 1 is wrong because...'
		},
		{
			answer: 'answer 2',
			correct: true,
			explanation: 'answer 2 is right because...'
		}
	]
}, {
	question:
	'What is another answer?',
	answers: [
		{
			answer: 'answer 1',
			correct: false,
			explanation: 'answer 1 is wrong because...'
		},
		{
			answer: 'answer 2',
			correct: false,
			explanation: 'answer 2 is wrong because...'
		},
		{
			answer: 'answer 3',
			correct: false,
			explanation: 'answer 3 is wrong because...'
		},
		{
			answer: 'answer 4',
			correct: true,
			explanation: 'answer 4 is right because...'
		}
	]
}, {
	question:
	'What is another answer?',
	answers: [
		{
			answer: 'answer 1',
			correct: false,
			explanation: 'answer 1 is wrong because...'
		},
		{
			answer: 'answer 2',
			correct: false,
			explanation: 'answer 2 is wrong because...'
		},
		{
			answer: 'answer 3',
			correct: false,
			explanation: 'answer 3 is wrong because...'
		},
		{
			answer: 'answer 4',
			correct: true,
			explanation: 'answer 4 is right because...'
		}
	]
}, {
	question:
	'What is another answer?',
	answers: [
		{
			answer: 'answer 1',
			correct: false,
			explanation: 'answer 1 is wrong because...'
		},
		{
			answer: 'answer 2',
			correct: false,
			explanation: 'answer 2 is wrong because...'
		},
		{
			answer: 'answer 3',
			correct: false,
			explanation: 'answer 3 is wrong because...'
		},
		{
			answer: 'answer 4',
			correct: true,
			explanation: 'answer 4 is right because...'
		}
	]
}, {
	question:
	'What is another answer?',
	answers: [
		{
			answer: 'answer 1',
			correct: false,
			explanation: 'answer 1 is wrong because...'
		},
		{
			answer: 'answer 2',
			correct: false,
			explanation: 'answer 2 is wrong because...'
		},
		{
			answer: 'answer 3',
			correct: false,
			explanation: 'answer 3 is wrong because...'
		},
		{
			answer: 'answer 4',
			correct: true,
			explanation: 'answer 4 is right because...'
		}
	]
}];